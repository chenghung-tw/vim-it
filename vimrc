"==============================================================================
"
" Vim configuration file.
"
" Author: Cheng-Hung Ko <August, 2006>
"
"==============================================================================

"*******************************************************************************
" Vundle (:help vundle-quickstart)
"

"
" Install Vundle plugin if it doesn't exist. Git and Curl must be available.
"
if has('win32')
    let s:bundle_path = expand('$VIM\vimfiles\bundle\')
else
    let s:bundle_path = expand('~/.vim/bundle/')
endif

let s:vundle_path = s:bundle_path . 'Vundle.vim'

if empty(glob(s:vundle_path)) " if Vundle doesn't exist, install it.
    silent exec
        \ "!git clone https://github.com/gmarik/Vundle.vim.git \""
        \ . s:vundle_path . "\""
endif

set nocompatible            " be iMproved
filetype off                " required!

" set the runtime path to include Vundle and initialize
let &runtimepath = &runtimepath . ',' . s:vundle_path
call vundle#begin(s:bundle_path)

" let Vundle manage Vundle, required
Plugin 'gmarik/Vundle.vim'

Plugin 'Mark--Karkat'
Plugin 'airblade/vim-gitgutter'
Plugin 'altercation/vim-colors-solarized'
Plugin 'craigemery/vim-autotag'
Plugin 'easymotion/vim-easymotion'
Plugin 'ervandew/supertab'
Plugin 'godlygeek/tabular'
Plugin 'honza/vim-snippets'
Plugin 'junegunn/gv.vim'
Plugin 'kien/ctrlp.vim'
Plugin 'majutsushi/tagbar'
Plugin 'matchit.zip'
Plugin 'mattesgroeger/vim-bookmarks'
Plugin 'mbbill/undotree'
Plugin 'mileszs/ack.vim'
Plugin 'scrooloose/nerdcommenter'
Plugin 'scrooloose/nerdtree'
Plugin 'scrooloose/syntastic'
Plugin 'sirver/ultisnips'
Plugin 'terryma/vim-multiple-cursors'
Plugin 'tpope/vim-fugitive'
Plugin 'tpope/vim-surround'
Plugin 'vim-airline/vim-airline'
Plugin 'vim-airline/vim-airline-themes'
Plugin 'visSum.vim'
Plugin 'will133/vim-dirdiff'

" All of your Plugins must be added before the following line
call vundle#end()           " required
filetype plugin indent on   " required


"*******************************************************************************
" Fonts and Shell
"
if has('win32')
    set shell=cmd.exe
    set shellcmdflag=/C
    "set guifont=Courier_New:h12:cDEFAULT
    set guifont=Bitstream_Vera_Sans_Mono:h10:cDEFAULT
elseif has('mac')
    set shell=/bin/bash
    set guifont=Monaco:h10
elseif has('unix')
    set shell=/bin/bash
    "set guifont=Bitstream\ Vera\ Sans\ Mono\ 10
    "set guifont=Fira\ Code\ 10
    set guifont=Hack\ 10
endif

let g:default_font_size = -1

function! ResizeFont(adjustment)
    if !has("gui_running")
        echom "Can't change font size in the terminal."
        return
    endif

    if has('unix')
        let l:pattern = '^\([a-zA-Z ]*\)\([0-9]\+\)$'
        let l:prefix = substitute(&guifont, l:pattern, '\1', '')
        let l:size = substitute(&guifont, l:pattern, '\2', '')
        let l:postfix = ''
    else
        let l:pattern = '^\(.*:h\)\([0-9]\+\)\(:.*\)$'
        let l:prefix = substitute(&guifont, l:pattern, '\1', '')
        let l:size = substitute(&guifont, l:pattern, '\2', '')
        let l:postfix = substitute(&guifont, l:pattern, '\3', '')
    endif

    " Save default font size
    if g:default_font_size <= 0
        let g:default_font_size = l:size
    endif

    if a:adjustment == 0 && g:default_font_size > 0
        let l:newsize = g:default_font_size     " restore to default font size
    else
        let l:newsize = l:size + a:adjustment   " adjust font size
    endif

    " Make sure the new font size value suitable
    let l:newsize = l:newsize >= 4 ? l:newsize : 4
    let l:newsize = l:newsize <= 32 ? l:newsize : 32

    if l:newsize != l:size
        let &guifont = l:prefix . l:newsize . l:postfix
        redraw
    endif

    echom "guifont = " . &guifont
endfunction

nnoremap <silent> <F10> :call ResizeFont(0)<CR>
nnoremap <silent> <F11> :call ResizeFont(-1)<CR>
nnoremap <silent> <F12> :call ResizeFont(1)<CR>


"*******************************************************************************
" Vim v.s. Gvim
"
if has('gui_running')
    "set number
    set background=dark
    set cursorline
    set guioptions+=b   " show bottom horizontal scrollbar
    set guioptions-=T   " hide toolbar
    set guioptions-=m   " hide menu bar

    " Color theme
    let g:solarized_diffmode="high"
    silent! colorscheme solarized

    " Airline status bar theme
    "let g:airline_solarized_bg = 'dark'
    "let g:airline_theme = 'solarized'
    let g:airline_theme = 'papercolor'
else
    " Color theme
    silent! colorscheme desert

    " Airline status bar theme
    set t_Co=256        " force VIM into 256 color mode
    let g:airline_theme = 'papercolor'
endif


"*******************************************************************************
" General Settings
"
let mapleader = ','
let maplocalleader = '\'
syntax on

"set autochdir
"set imdisable
set autoindent
set backspace=indent,eol,start
set encoding=utf-8
set fileencoding=utf-8
set formatoptions+=c,r,o,q,j
set hlsearch
set ignorecase
set incsearch
set laststatus=2
set mouse=a
set nocompatible
set noswapfile
set nowrap
set nowrapscan
set ruler
set showcmd
set smartcase
set splitbelow                          " let preview window to split below
set wildmenu

if v:version >= 800
    set belloff=all                     " no bell at all
else
    set novisualbell
    set noerrorbells
endif

"
" Folding
"
set foldmethod=syntax
set nofoldenable

"
" Tab label format
"
set guitablabel=%N\ %t\ %M

"
" Cscope
"
"   ctags: forward lookup, omni-completion
"   cscope: forward lookup, backward lookup
"
if filereadable("cscope.out")
    cs add cscope.out
endif

" Use the default :tag, instead of the :cstag, which will prompt the users to
" select one tag if multiple matches are found.
set nocscopetag

" Order in which :cstag performs a search. 0: cscope first; 1: ctags first
set csto=1

" Use quickfix window and clear previous result.
set cscopequickfix=s-,c-,d-,i-,t-,e-

nnoremap <C-\>s :cs find s <C-R>=expand("<cword>")<CR><CR>
nnoremap <C-\>g :cs find g <C-R>=expand("<cword>")<CR><CR>
nnoremap <C-\>c :cs find c <C-R>=expand("<cword>")<CR><CR>
nnoremap <C-\>t :cs find t <C-R>=expand("<cword>")<CR><CR>
nnoremap <C-\>e :cs find e <C-R>=expand("<cword>")<CR><CR>
nnoremap <C-\>f :cs find f <C-R>=expand("<cfile>")<CR><CR>
nnoremap <C-\>i :cs find i ^<C-R>=expand("<cfile>")<CR>$<CR>
nnoremap <C-\>d :cs find d <C-R>=expand("<cword>")<CR><CR>
nnoremap <C-\>a :cs find a <C-R>=expand("<cword>")<CR><CR>

"
" General Highlights
"
" Popup menu
highlight Pmenu ctermfg=Black ctermbg=Gray cterm=None
highlight PmenuSel ctermfg=Black ctermbg=Yellow cterm=Bold

" Quickfix and location list
highlight QuickFixLine ctermfg=None ctermbg=None cterm=reverse

" Search
highlight Search ctermfg=Black ctermbg=Yellow cterm=Bold
highlight IncSearch ctermfg=Black ctermbg=LightYellow cterm=Bold


"*******************************************************************************
" Plugin: vim-snippets and ultisnips
"
" Trigger configuration.
" Do not use <tab> if you use " https://github.com/Valloric/YouCompleteMe.
"let g:UltiSnipsExpandTrigger="<tab>"
"let g:UltiSnipsJumpForwardTrigger="<c-b>"
"let g:UltiSnipsJumpBackwardTrigger="<c-z>"

" If you want :UltiSnipsEdit to split your window.
"let g:UltiSnipsEditSplit="vertical"


"*******************************************************************************
" Plugin: vim-autotag
"
let g:autotagTagsFile = 'tags'


"*******************************************************************************
" Plugin: tagbar
"
highlight TagbarHighlight ctermfg=None ctermbg=None cterm=reverse

let g:tagbar_autofocus = 0          " focus on tagbar or not after it opens
let g:tagbar_autoclose = 0          " don't close tagbar after clicking a tag
let g:tagbar_indent = 2             " number of spaces for each folding level
let g:tagbar_left = 1               " 1: left; 0: right
let g:tagbar_sort = 0               " 1: by name; 0: by order
let g:tagbar_width = 36

nnoremap <silent> <LocalLeader>t :TagbarToggle<CR>


"*******************************************************************************
" Plugin: NERDTree
"
let g:NERDTreeWinPos = "right"  " Put NERDTree on the right side.
let g:NERDTreeWinSize = 36

nnoremap <silent> <LocalLeader>d :NERDTreeToggle<CR>
nnoremap <silent> <LocalLeader>f :NERDTreeFind<CR>


"*******************************************************************************
" Plugin: undotree
"
nnoremap <silent> <LocalLeader>u :UndotreeToggle<CR>


"*******************************************************************************
" Plugin: Syntastic
"
let g:syntastic_mode_map = {
    \ "mode": "passive",
    \ "active_filetypes": ["python", "xml"],
    \ "passive_filetypes": [] }

let g:syntastic_always_populate_loc_list = 1
"let g:syntastic_auto_loc_list = 2


"*******************************************************************************
" Plugin: vim-gitgutter
"
"   jump to next hunk (change): ]c
"   jump to previous hunk (change): [c.
"   turn off with :GitGutterDisable
"   turn on with :GitGutterEnable
"   toggle with :GitGutterToggle

let g:gitgutter_enabled = 0
let g:gitgutter_map_keys = 0

nnoremap <silent> <Leader>g :GitGutterToggle<CR>
nmap [c <Plug>(GitGutterPrevHunk)
nmap ]c <Plug>(GitGutterNextHunk)


"*******************************************************************************
" Plugin: vim-bookmarks
"
" Disable the following default key mapping.
"   Add/remove bookmark at current line          mm   :BookmarkToggle
"   Add/edit/remove annotation at current line   mi   :BookmarkAnnotate <TEXT>
"   Jump to next bookmark in buffer              mn   :BookmarkNext
"   Jump to previous bookmark in buffer          mp   :BookmarkPrev
"   Show all bookmarks (toggle)                  ma   :BookmarkShowAll
"   Clear bookmarks in current buffer only       mc   :BookmarkClear
"   Clear bookmarks in all buffers               mx   :BookmarkClearAll
"let g:bookmark_no_default_key_mappings = 1


"*******************************************************************************
" Plugin: ctrlp
"
let g:ctrlp_user_command = {
    \ 'types': {
        \ 1: ['vimit.out', 'cat %s/vimit.out'],
        \ 2: ['.git', 'cd %s && git ls-files'],
    \ },
\ }

if executable('ag')
    let g:ctrlp_user_command.fallback = 'ag --nocolor -g "" %s'
endif

let g:ctrlp_working_path_mode = ''          " don't change working directory
let g:ctrlp_by_filename = 1
let g:ctrlp_root_markers = ['vimit.out']
let g:ctrlp_use_caching = 1
let g:ctrlp_regexp = 0
let g:ctrlp_max_depth = 4
let g:ctrlp_max_files = 300


"*******************************************************************************
" Plugin: Mark
"

" If you don't want the * and # mappings remember the last search type and
" instead always search for the next occurrence of the current mark, with a
" fallback to Vim's original * command
"nnoremap * <Plug>MarkSearchOrCurNext
"nnoremap # <Plug>MarkSearchOrCurPrev

" Clear all marks.
nnoremap <Leader>M :MarkClear<CR>

if has('gui_running')
    let g:mwDefaultHighlightingPalette = 'extended'
endif


"*******************************************************************************
" Plugin: DirDiff
"
let g:DirDiffEnableMappings = 0
let g:DirDiffExcludes = 'CVS,.git,*.o,*.class,*.exe,*.swp,*.bin,*.pyc'
let g:DirDiffTheme = "solarized"

function! CopyToLeft()
    if &diff
        let l:winid = winnr()
        if l:winid == 2
            exec "diffget"
        elseif l:winid == 3
            exec "diffput"
        endif
    endif
endfunction

function! CopyToRight()
    if &diff
        let l:winid = winnr()
        if l:winid == 2
            exec "diffput"
        elseif l:winid == 3
            exec "diffget"
        endif
    endif
endfunction

function! GoToPrevDiff()
    if &diff
        exec "DirDiffPrev"
        exec 2 . "wincmd w"
    endif
endfunction

function! GoToNextDiff()
    if &diff
        exec "DirDiffNext"
        exec 2 . "wincmd w"
    endif
endfunction

function! SwitchDiffWindow()
    if &diff
        let l:winid = winnr()
        if l:winid == 2
            exec 3 . "wincmd w"
        elseif l:winid == 3
            exec 2 . "wincmd w"
        endif
    endif
endfunction

noremap <silent> <C-Up> [c
noremap <silent> <C-Down> ]c
noremap <silent> <C-Left> :call CopyToLeft()<CR>
noremap <silent> <C-Right> :call CopyToRight()<CR>
noremap <silent> <C-PageUp> :call GoToPrevDiff()<CR>
noremap <silent> <C-PageDown> :call GoToNextDiff()<CR>
noremap <silent> <C-Tab> :call SwitchDiffWindow()<CR>

function! DirDiffCustomize()
    if &diff
        set nocursorline
    endif
endfunction

augroup dir_diff
    autocmd!
    autocmd FilterWritePre * call DirDiffCustomize()
augroup END


"*******************************************************************************
" Plugin: ACK
"

"let g:ackhighlight = 1

if executable('ag')
    let g:ackprg = 'ag --vimgrep'
endif


"*******************************************************************************
" Vim help 'CursorHold-example': display tag in preview window
"
let g:preview_tag_enable = 0        " default disable

function! IsPreviewWindowExisted()
    for i in range(1, winnr('$'))
        if getwinvar(i, "&previewwindow")
            return 1
        endif
    endfor

    return 0
endfunction

function! IsQuickfixOrLocationListExisted()
    for i in range(1, winnr('$'))
        if getwinvar(i, "&buftype") == "quickfix"
            return 1
        endif
    endfor

    return 0
endfunction

function! IsValidTagNaming(name)
    return a:name =~ '^[_a-zA-Z][_a-zA-Z0-9]*'
endfunction

" TODO: fixing co-existence issue of preview, quickfix and location list.
"
"   a) If preview, quickfix and location list are open at the same time,
"      window height of preview window might not be restored when closing
"      quickfix or location list.
"   b) Press <Enter> on quickfix window or location list could jump to the
"      preview window, rather than the main editing window.
"
function! PreviewWord(reload)
    " Skip in diff mode, preview window, quickfix or location list
    if g:preview_tag_enable == 0 || &diff || &previewwindow
        \ || IsQuickfixOrLocationListExisted()
        return
    endif

    let w = expand("<cword>") " get the word under cursor

    if a:reload
        if !IsValidTagNaming(w)
            if exists('t:latest_good_tag')
                let w = t:latest_good_tag
            else
                return
            endif
        endif
    else
        if !IsValidTagNaming(w)
            return
        endif

        if exists('t:latest_good_tag') && w == t:latest_good_tag
            return
        endif
    endif

    " Try displaying a matching tag for the word under the cursor
    try
        exec "ptag " . w
    catch
        return
    endtry

    silent! wincmd P                " jump to preview window
    if &previewwindow               " if we really get there...
        let t:latest_good_tag = w

        if has("folding")
            silent! .foldopen       " don't want a closed fold
        endif

        " Put cursor at the center of the screen
        exec 'normal zz'

        "
        " Add a match highlight to the word at this position
        "
        match none                  " delete existing highlight
        call search("$", "b")       " to end of previous line

        " Backslash is magic and should be escaped.
        let w = substitute(w, '\\', '\\\\', "")

        " Position cursor on match
        "   \<: beginning of the word
        "   \>: end of the word
        "   \V: except backsplash, all characters after this are nomagic
        call search('\<\V' . w . '\>')

        " Looks like this: 'match previewWorld "\%12l\%34c\k*"
        highlight previewWord term=bold ctermbg=Yellow guibg=Yellow
        exec 'match previewWord "\%' . line(".") . 'l\%' . col(".") . 'c\k*"'

        wincmd p                    " back to old window
    endif
endfunction

function! PreviewTagEnable()
    if !filereadable('tags')
        echom "tags doesn't exist. Can't use the tag preview."
        return
    endif

    let g:preview_tag_enable = 1
    call PreviewWord(1)
endfunction

function! PreviewTagToggle()
    if g:preview_tag_enable == 1
        let g:preview_tag_enable = 0
        pclose
    else
        call PreviewTagEnable()
    endif
endfunction

"augroup preview_tags
    "autocmd!
    "if filereadable('tags')    " preview codes only if 'tags' file exists
        "set updatetime=200     " milliseconds that CursorHold event triggers
        "autocmd CursorHold *.c,*.cpp,*.h,*.java nested call PreviewWord(0)
    "endif
"augroup END

nnoremap <silent> <LocalLeader>p :call PreviewTagToggle()<CR>
nnoremap <silent> <LeftRelease> :call PreviewWord(0)<CR>
nnoremap <silent> <Space> :call PreviewWord(0)<CR>


"*******************************************************************************
" Vim help 'setting-tabline': display tabline in terminal
"
function! MyTabLabel(n)
    let buflist = tabpagebuflist(a:n)
    let winnr = tabpagewinnr(a:n)
    return fnamemodify(bufname(buflist[winnr - 1]), ":t")
endfunction

function! MyTabLine()
    let s = ''
    for i in range(tabpagenr('$'))
        " select the highlighting
        if i + 1 == tabpagenr()
            highlight currentTab term=bold ctermbg=DarkBlue ctermfg=Yellow
            let s .= '%#currentTab#'
        else
            let s .= '%#TabLine#'
        endif

        " set the tab page number (for mouse clicks)
        let s .= '%' . (i + 1) . 'T'

        " the label is made by MyTabLabel()
        let s .= '[' . (i + 1) . ' %{MyTabLabel(' . (i + 1) . ')}]'
    endfor

    " after the last tab fill with TabLineFill and reset tab page nr
    let s .= '%#TabLineFill#%T'

    " right-align the label to close the current tab page
    if tabpagenr('$') > 1
        let s .= '%=%#TabLine#%999Xclose'
    endif

    return s
endfunction

set tabline=%!MyTabLine()


"*******************************************************************************
" Tip #14: Highlight matches without moving
"
let g:highlighting = 0

function! Highlighting()
    if g:highlighting == 1 && @/ =~ '^\\<'.expand('<cword>').'\\>$'
        let g:highlighting = 0
        return ":silent nohlsearch\<CR>"
    endif
    let @/ = '\<'.expand('<cword>').'\>'
    let g:highlighting = 1
    return ":silent set hlsearch\<CR>"
endfunction

nnoremap <silent> <expr> <Leader>h Highlighting()


"*******************************************************************************
" Tip #93: if you use 'highlight search' feature, map a key to :noh
"
nnoremap i :nohlsearch<CR>i
nnoremap I :nohlsearch<CR>I
nnoremap a :nohlsearch<CR>a
nnoremap A :nohlsearch<CR>A
nnoremap o :nohlsearch<CR>o
nnoremap O :nohlsearch<CR>O


"*******************************************************************************
" Tip #431: Change between backslash and forward slash
"
function! ToggleSlash(line1, line2)
    let l:from = ''
    for l:lnum in range(a:line1, a:line2)
        let l:line = getline(l:lnum)
        let l:first = matchstr(l:line, '[/\\]')
        if !empty(l:first)
            if empty(l:from)
                let l:from = l:first
            endif
            let opposite = (l:from == '/' ? '\' : '/')
            call setline(l:lnum, substitute(l:line, l:from, opposite, 'g'))
        endif
    endfor
endfunction

command! -range=% ToggleSlash call ToggleSlash(<line1>, <line2>)


"*******************************************************************************
" Tip #600: Copy filename to clipboard.
"
" ,fr copies the relative file path to the current directory
" ,fa copies the absolute file path
" ,ft copies the tail filename only
" ,fh copies the head (last path component removed)
"

" Convert slashes to backslashes for Windows.
if has('win32')
    nnoremap <Leader>fr :let @*=substitute(expand("%"), "/", "\\", "g")<CR>
    nnoremap <Leader>fa :let @*=substitute(expand("%:p"), "/", "\\", "g")<CR>
    nnoremap <Leader>ft :let @*=substitute(expand("%:t"), "/", "\\", "g")<CR>
    nnoremap <Leader>fh :let @*=substitute(expand("%:h"), "/", "\\", "g")<CR>
else
    nnoremap <Leader>fr :let @+=expand("%")<CR>
    nnoremap <Leader>fa :let @+=expand("%:p")<CR>
    nnoremap <Leader>ft :let @+=expand("%:t")<CR>
    nnoremap <Leader>fh :let @+=expand("%:h")<CR>
endif


"*******************************************************************************
" Tip #646: moving lines up/down in a file
"

" move the current line up or down
nnoremap <S-Down> :m+<CR>==
nnoremap <S-Up> :m-2<CR>==
inoremap <S-Down> <C-O>:m+<CR><C-O>==
inoremap <S-Up> <C-O>:m-2<CR><C-O>==

" move the current line left or right
nnoremap <S-Left> <<
nnoremap <S-Right> >>
inoremap <S-Left> <C-O><<
inoremap <S-Right> <C-O>>>

" move the selected block up or down
vnoremap <S-Down> :m'>+<CR>gv=gv
vnoremap <S-Up> :m'<-2<CR>gv=gv

" move the selected block left or right
vnoremap <S-Right> >gv
vnoremap <S-Left> <gv


"*******************************************************************************
" Tip #1221: Alternative tab navigation
"

" tabe: open new tab
" tabc: close tab
" gt: go to the next tab
" gT: go to the preview tab
" \dgt: go to the \d-th tab
" \dtabo: only keep \d-th tab
" \dtabm: move to the \d-th tab


"*******************************************************************************
" Tip #1672: Searching for expressions which include slash
"

"
" Searching for slash as normal text
"
" Ss allows easy searching for text which includes slashes. For example:
" :Ss /abc/def/ghi/ Set search register (@/) to '/abc/def/ghi/'
" n                 Search for next occurrence of text in search register.
"

"command! -nargs=1 Ss let @/ = <q-args>
"command! -nargs=1 Ss let @/ = <q-args>|set hlsearch
command! -nargs=1 Ss let @/ = escape(<q-args>, '/')|normal! /<C-R>/<CR>

"
" Searching for all characters as normal text
"
" SS which allows easy searching for text which includes characters that
" normally have a special meaning in a search pattern. For example:
"
" :SS ^abc/def\[ghi\]x*y    Set search register to '\V^abc/def\\[ghi\\]x*y'.
" n                         Search for next occurrence

"command! -nargs=1 SS let @/ = '\V'.escape(<q-args>, '\')
"command! -nargs=1 SS let @/ = '\V'.escape(<q-args>, '\')|set hlsearch
command! -nargs=1 SS let @/ = '\V'.escape(<q-args>, '/\')|normal! /<C-R>/<CR>


"*******************************************************************************
" Tip by CH: Edit and source vimrc
"
nnoremap <leader>ev :edit $MYVIMRC<CR>
nnoremap <leader>sv :source $MYVIMRC<CR>


"*******************************************************************************
" Tip by CH: Switch between windows
"
nnoremap <silent> <leader>1 :1wincmd w<CR>
nnoremap <silent> <leader>2 :2wincmd w<CR>
nnoremap <silent> <leader>3 :3wincmd w<CR>
nnoremap <silent> <leader>4 :4wincmd w<CR>
nnoremap <silent> <leader>5 :5wincmd w<CR>
nnoremap <silent> <leader>6 :6wincmd w<CR>
nnoremap <silent> <leader>7 :7wincmd w<CR>
nnoremap <silent> <leader>8 :8wincmd w<CR>
nnoremap <silent> <leader>9 :9wincmd w<CR>
nnoremap <silent> <leader>0 :0wincmd w<CR>

nnoremap <silent> <A-Up> :wincmd k<CR>
nnoremap <silent> <A-Down> :wincmd j<CR>
nnoremap <silent> <A-Right> :wincmd l<CR>
nnoremap <silent> <A-Left> :wincmd h<CR>


"*******************************************************************************
" Tip by CH: Special jumpping shortcut for programming windows
"

" Jump to the tagbar window if already open
nnoremap <silent> <Leader>t :TagbarOpen j<CR>

" Jump to the NERDTree window if already open
nnoremap <silent> <Leader>d :NERDTreeFocus<CR>

" Jump to the preview window if already open
function! JumpToPreviewWindow()
    call PreviewTagEnable()
    silent! wincmd P                " jump to preview window
endfunction

nnoremap <silent> <Leader>p :call JumpToPreviewWindow()<CR>

" Jump to the main working window if existed
function! JumpToWorkingWindow()
    for l:i in range(1, winnr('$'))
        if !strlen(getwinvar(l:i, "&buftype"))
            \ && !getwinvar(l:i, "&previewwindow")
            exec l:i . "wincmd w"
            return
        endif
    endfor
endfunction

nnoremap <silent> <Leader>w :call JumpToWorkingWindow()<CR>


"*******************************************************************************
" Tip by CH: Trim whitespace at the end of the line.
"
function! TrimTails(line1, line2)
    let l:pos = getpos(".")     " save cursor position

    try
        exec a:line1. ',' . a:line2 's/\s\+$//'
    catch /E486:/
        echom "Can't find trailing spaces."
    endtry

    call setpos(".", l:pos)     " restore cursor position
endfunction

command! -range=% TrimTails call TrimTails(<line1>, <line2>)


"*******************************************************************************
" Tip by CH: Pasting code with syntax coloring in emails
"
function! Kobe(line1, line2)
    " backup original color scheme and use the default to copy codes
    let l:colorscheme = g:colors_name
    let l:background = &background
    colorscheme default

    exec a:line1.','.a:line2.'TOhtml'

    " restore original color scheme
    exec "colorscheme " . l:colorscheme
    let &background = l:background
endfunction

command! -range Kobe call Kobe(<line1>, <line2>)


"*******************************************************************************
" Tip by CH: spell check for git commit message.
"
augroup spell_check
    autocmd!
    autocmd FileType gitcommit SpellCheckToggle
augroup END

command! -nargs=0 SpellCheckToggle setlocal spell! spelllang=en_us


"*******************************************************************************
" Tip by CH: Coding styles for spaces and indentations
"
let g:kernel_coding_style = -1  " not detected yet

function! SetDefaultSpaceAndIndentation()
    setlocal cindent
    setlocal expandtab          " expand tab by using spaces
    setlocal shiftwidth=4       " spaces used for auto indentation
    setlocal softtabstop=4      " spaces used for tab when editing
    setlocal tabstop=8          " what a real tab looks like in the editor
endfunction

function! SetKernelSpaceAndIndentation()
    "setlocal cinoptions=:0,l1,t0,g0,(0
    setlocal cindent
    setlocal noexpandtab
    setlocal shiftwidth=8
    setlocal softtabstop=8
    setlocal tabstop=8
endfunction

function! SetSpaceAndIndentation()
    if g:kernel_coding_style < 0
        if expand('%:p') =~ '/linux\|/kernel' ||
            \ (filereadable('vimit.out') &&
            \   match(readfile('vimit.out'), 'linux') >= 0)
            let g:kernel_coding_style = 1
        else
            let g:kernel_coding_style = 0
        endif
    endif

    if g:kernel_coding_style
        call SetKernelSpaceAndIndentation()
    else
        call SetDefaultSpaceAndIndentation()
    endif
endfunction

augroup space_and_indentation
    autocmd!
    autocmd FileType c,cpp,h call SetSpaceAndIndentation()
    autocmd FileType java,perl,python,vim call SetDefaultSpaceAndIndentation()
    autocmd FileType kconfig,dts,diff call SetKernelSpaceAndIndentation()
augroup END


"*******************************************************************************
" Tip by CH: Highlight and auto-wrap for text width
"
function! SetColorColumn(width, always, autowrap)
    let &l:textwidth = a:width

    if has('gui_running') || a:always
        let &l:colorcolumn = join(range(120), ',+')[2:]
    endif

    if a:autowrap
        setlocal formatoptions+=t
    endif
endfunction

augroup text_width_color_column
    autocmd!
    autocmd FileType c,cpp,perl,python,vim call SetColorColumn(80, 0, 0)
    autocmd FileType java call SetColorColumn(100, 0, 0)
    autocmd FileType gitcommit call SetColorColumn(75, 1, 1)
augroup END


"*******************************************************************************
" Tip #911: Make views automatic
"
set viewoptions-=options

augroup restore_file_state
    autocmd!
    autocmd BufWinLeave * if expand("%") != "" | mkview | endif
    autocmd BufWinEnter * if expand("%") != "" | silent loadview | endif
augroup END


"*******************************************************************************
" Tip by CH: Programming Session Management
"
set sessionoptions-=options

function! SaveSession()
    tabdo NERDTreeClose
    tabdo TagbarClose
    tabdo pclose
    mksession! session.vim
endfunction

function! LoadSession()
    if filereadable('session.vim')
        source session.vim
        tabdo NERDTree
        tabdo TagbarOpen
        tabdo 2wincmd w
    endif
endfunction

augroup session_management
    autocmd!

    if filereadable('vimit.out')
        autocmd VimLeave * silent SaveSession
    endif

    "if filereadable('session.vim')
        "autocmd VimEnter * silent LoadSession
    "endif
augroup END

command! -nargs=0 SaveSession call SaveSession()
command! -nargs=0 LoadSession call LoadSession()


"*******************************************************************************
" Tip by CH: Local search & global search for the whole project
"
" Global search. If ack or silver searcher is available, use 'Ack' command to
" grep the pattern. Otherwise, use traditional 'grep' command.
"
function! GrepProjectFiles(pattern, isLocal)
    let l:cmd = ''

    " Search without using regular expression
    "   vimgrep: \V and escape the slashes and backslashes
    "   silver search: -Q
    if a:isLocal
        let l:cmd = 'lvimgrep /\V' . escape(a:pattern, '/\') . '/ %'
    else
        if !filereadable('vimit.out')
            echom "vimit.out doesn't exist! Build it before using vim-it."
            return
        endif

        if !executable('ack') && !executable('ag')
            echom "ack or silver searcher isn't available."
            return
        endif

        " # and % are magic and will be expanded to filename, etc. Need
        " to escape them.
        let l:w = a:pattern
        let l:w = substitute(l:w, '#', '\\#', "g")
        let l:w = substitute(l:w, '%', '\\%', "g")

        let l:cmd = 'LAckAdd -Q "' . l:w . '" `cat vimit.out`'
    endif

    exec l:cmd

    exec 'botright lopen'
    exec 'wincmd p'
    exec 'pclose'
endfunction

command! -nargs=1 GlobalGrep call GrepProjectFiles(<q-args>, 0)
nnoremap <C-g> :GlobalGrep<space>

command! -nargs=1 LocalGrep call GrepProjectFiles(<q-args>, 1)
nnoremap <C-f> :LocalGrep<space>

"
" Shortcuts to walk through the items in the quickfix or location window.
"
nnoremap ]q :cnext<CR>
nnoremap [q :cprev<CR>
nnoremap ]Q :clast<CR>
nnoremap [Q :cfirst<CR>

nnoremap ]a :lnext<CR>
nnoremap [a :lprev<CR>
nnoremap ]A :llast<CR>
nnoremap [A :lfirst<CR>


"*******************************************************************************
" Tip by CH: Full window
"
function! FullWindowToggle(vertical, horizontal)
    if !exists('w:window_size')
        let w:window_size = []
    endif

    if empty(w:window_size)
        for l:i in range(1, winnr('$'))
            call add(w:window_size, [winwidth(l:i), winheight(l:i)])
        endfor

        if a:vertical
            exec 'resize'
        endif

        if a:horizontal
            exec 'vertical resize'
        endif
    else
        let l:current = winnr()

        let l:copied_size = copy(w:window_size)

        for l:i in range(0, len(l:copied_size) - 1)
            let l:width = l:copied_size[l:i][0]
            let l:height = l:copied_size[l:i][1]
            exec l:i + 1 . 'wincmd w'
            exec 'vertical resize ' . l:width
            exec 'resize ' . l:height
        endfor

        for l:i in range(0, len(l:copied_size) - 1)
            let l:width = l:copied_size[l:i][0]
            let l:height = l:copied_size[l:i][1]
            exec l:i + 1 . 'wincmd w'
            exec 'vertical resize ' . l:width
            exec 'resize ' . l:height
        endfor

        exec l:current . 'wincmd w'
        let w:window_size = []
    endif
endfunction

function! FullWindowReset()
    if exists('w:window_size') && !empty(w:window_size)
        call FullWindowToggle(0, 0)   " paremeters are dummy
    endif
endfunction

nnoremap <silent> <Leader>X :call FullWindowToggle(1, 1)<CR>
nnoremap <silent> <Leader>H :call FullWindowToggle(0, 1)<CR>
nnoremap <silent> <Leader>V :call FullWindowToggle(1, 0)<CR>

augroup full_window
    autocmd!
    autocmd WinLeave * call FullWindowReset()
augroup END


"*******************************************************************************
" Tip by CH: Helper of the quickfix and location window
"
"
" Toggle
"
function! RestorePreview()
    for i in range(1, winnr('$'))
        " Try to find the window with normal buffer and jump to it.
        if getwinvar(i, "&buftype") == ""
            exec i . 'wincmd w'
            break
        endif
    endfor

    call PreviewWord(1)
endfunction

function! ToggleQuickfix()
    let l:num = winnr('$')
    exec 'cclose'

    if l:num > winnr('$')         " Quickfix is closed
        call RestorePreview()     " Try to preview word
    else                          " Try to open quickfix
        exec 'botright copen'

        if l:num < winnr('$')     " Quickfix opens successfully.
            exec 'wincmd p'
            exec 'pclose'
        endif
    endif
endfunction

function! ToggleLocation()
    let l:num = winnr('$')
    exec 'lclose'

    if l:num > winnr('$')         " Location list is closed
        call RestorePreview()     " Try to preview word
    else                          " Try to open location list
        try
            exec 'botright lopen'
        catch /E776:/
            echom "No location list."
        endtry

        if l:num < winnr('$')    " Location list opens successfully.
            exec 'wincmd p'
            exec 'pclose'
        endif
    endif
endfunction

nnoremap <silent> <Localleader>a :call ToggleLocation()<CR>
nnoremap <silent> <Localleader>q :call ToggleQuickfix()<CR>

"
" Jump
"
function! JumpToQuickFixWindow()
    let l:num = winnr('$')

    exec 'botright copen'

    if l:num < winnr('$')     " Quickfix opens successfully.
        exec 'wincmd p'
        exec 'pclose'
    endif

    for l:i in range(1, winnr('$'))
        if getwininfo(win_getid(l:i))[0]['quickfix'] == 1
            \ && getwininfo(win_getid(l:i))[0]['loclist'] == 0
            exec l:i . "wincmd w"
            return
        endif
    endfor
endfunction

function! JumpToLocationListWindow()
    let l:num = winnr('$')

    try
        exec 'botright lopen'
    catch /E776:/
        echom "No location list."
    endtry

    if l:num < winnr('$')    " Location list opens successfully.
        exec 'wincmd p'
        exec 'pclose'
    endif

    for l:i in range(1, winnr('$'))
        if getwininfo(win_getid(l:i))[0]['loclist'] == 1
            exec l:i . "wincmd w"
            return
        endif
    endfor
endfunction

nnoremap <silent> <Leader>q :call JumpToQuickFixWindow()<CR>
nnoremap <silent> <Leader>a :call JumpToLocationListWindow()<CR>

"
" Reset
"
function! ResetQuickFix()
    exec 'cexpr []'
    exec 'cclose'
    call RestorePreview()     " Try to preview word
endfunction

function! ResetLocation()
    exec 'lexpr []'
    exec 'lclose'
    call RestorePreview()     " Try to preview word
endfunction

nnoremap []q :call ResetQuickFix()<CR>
nnoremap []a :call ResetLocation()<CR>


"*******************************************************************************
" Source others
"
if has('win32')
    if filereadable($VIM . '\_vimrc_grep')
        source $VIM\_vimrc_grep
    endif
else
    if !empty(glob('~/.vimrc_grep'))
        source ~/.vimrc_grep
    endif
endif
