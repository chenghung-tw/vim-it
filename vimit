#!/usr/bin/python
#
# Author: Cheng-Hung Ko

import errno
import os
import os.path
import re
import shutil
import sys
import tempfile


################################################################################
# Global
#

def hSize(nbytes):
    suffixes = ['B', 'KB', 'MB', 'GB']

    if nbytes == 0:
        return '0 B'

    i = 0
    while nbytes >= 1024 and i < len(suffixes) - 1:
        nbytes /= 1024.
        i += 1

    f = ('%.2f' % nbytes).rstrip('0').rstrip('.')

    return "{} {}".format(f, suffixes[i])


################################################################################
# Class
#

class Installer(object):
    def __init__(cls, folder):
        object.__init__(cls)
        cls.folder = folder

    def install(cls):
        return cls.installVimrc()

    def installVimrc(cls):
        path = os.path.join(cls.folder, '.vimrc')
        if not cls.isUnix():
            path = os.path.join(cls.folder, '_vimrc')

        if cls.confirmOverride(path):
            shutil.copy('vimrc', path)
            return True

        return False

    def isUnix(cls):
        items = os.listdir(cls.folder)
        beginWithDot = [i for i in items if re.match('\.', i)]

        if beginWithDot:
            return True
        else:
            return False

    def confirmOverride(cls, path):
        if os.path.isfile(path):
            ans = raw_input("Override existing {}? (y/N) ".format(path))
            if ans != 'Y' and ans != 'y':
                return False

        return True


class Builder(object):
    def __init__(cls, entries):
        object.__init__(cls)
        cls.entries = entries
        cls.files = []

    def build(cls):
        cls.traverseProject()
        return cls.buildVimit() + cls.buildTags()

    @staticmethod
    def getVimitFile():
        return 'vimit.out'

    @staticmethod
    def getCtagsFile():
        return 'tags'

    @staticmethod
    def getCscopeFiles():
        return ['cscope.in.out', 'cscope.out', 'cscope.po.out']

    @staticmethod
    def getSessionFile():
        return 'session.vim' # Session management file. Check vimrc for detail.

    @staticmethod
    def getProjFiles():
        ret = [Builder.getVimitFile()]
        ret.append(Builder.getCtagsFile())
        ret += Builder.getCscopeFiles()
        ret.append(Builder.getSessionFile())

        return ret

    @staticmethod
    def clean():
        print "Clean project"
        try:
            totalSize = 0
            for f in Builder.getProjFiles():
                if os.path.isfile(f):
                    size = os.path.getsize(f)
                    print "  {}: {}".format(f, hSize(size))
                    os.remove(f)
                    totalSize += size

            return totalSize
        except OSError as e:
            if e.errno != errno.ENOENT:
                raise e

    def getSupportFileTypes(cls):
        return ['java', 'xml', 'py', 'c', 'h', 'cpp', 'hpp', 'rc', 'conf', 'hal']

    def getSupportFileTypesRegexp(cls):
        filetypes = cls.getSupportFileTypes()
        regexp = '\.({}'.format(filetypes[0])

        if len(filetypes) >= 2:
            for t in filetypes[1:]:
                regexp += "|{}".format(t)

        regexp += ")$"

        return regexp

    def dumpFileTypeStatistics(cls):
        filetypes = cls.getSupportFileTypes()

        for t in filetypes:
            regexp = '\.{}$'.format(t)
            num = len([f for f in cls.files if re.search(regexp, f)])
            print "  {}: {} file(s)".format(t, num)

    def checkToAddFile(cls, dirPath, filename):
        path = os.path.join(dirPath, filename)
        if os.path.isfile(path) and not os.path.islink(path):
            cls.files.append(path)

    def traverseFolder(cls, folder):
        regexp = cls.getSupportFileTypesRegexp()

        for dirPath, dirNames, filenames in os.walk(folder):
            # Modify dirNames in-place to exclude folders started with '.'
            dirNames[:] = [d for d in dirNames if not re.match('\.', d)]

            # Only keep supported file types.
            candidates = [f for f in filenames if re.search(regexp, f)]

            for f in candidates:
                cls.checkToAddFile(dirPath, f)

    def traverseProject(cls):
        print "Scan project files"

        for entry in cls.entries:
            if os.path.isdir(entry):
                cls.traverseFolder(entry)
            else:
                cls.checkToAddFile('', entry)

        cls.files.sort(key=os.path.basename)
        cls.dumpFileTypeStatistics()

    def buildVimit(cls):
        print "\nBuild vimit"

        totalSize = 0

        with open(cls.getVimitFile(), 'w') as fh:
            for f in cls.files:
                fh.write(f + "\n");

        size = os.path.getsize(cls.getVimitFile())
        print "  {}: {}".format(cls.getVimitFile(), hSize(size))
        totalSize += size

        return totalSize

    def buildTags(cls):
        tmpFile = tempfile.NamedTemporaryFile(delete=False)
        with open(tmpFile.name, 'w') as fh:
            for f in cls.files:
                fh.write(f + "\n")

        totalSize = 0

        print "\nBuild ctags"
        os.system("ctags -R -L " + tmpFile.name)
        if os.path.isfile(cls.getCtagsFile()):
            totalSize = os.path.getsize(cls.getCtagsFile())
            print "  {}: {}".format(cls.getCtagsFile(), hSize(totalSize))

        print "\nBuild cscope"
        os.system("cscope -Rbq -i " + tmpFile.name)
        for f in cls.getCscopeFiles():
            if os.path.isfile(f):
                size = os.path.getsize(f)
                print "  {}: {}".format(f, hSize(size))
                totalSize += size

        os.remove(tmpFile.name)

        return totalSize

class ArgumentParser(object):
    def __init__(cls, argv):
        object.__init__(cls)

        cls.message = ToolManual
        cls.argv = {
            'action': '',
            'project-entries': [],
            'install-folder': '',
        }

        if len(argv) <= 1:
            return

        if argv[1] == 'build':
            if len(argv) <= 2:
                return

            cls.argv['project-entries'] = argv[2:]
            cls.argv['action'] = 'build'
        elif argv[1] == 'install':
            if not os.path.exists('vimrc'):
                cls.message = "Fail. Can't find vimrc.\n" + \
                               "Make sure you are in the vim-it folder.\n"
                return

            if len(argv) != 3 or not os.path.isdir(argv[2]):
                return

            cls.argv['install-folder'] = argv[2]
            cls.argv['action'] = 'install'
        elif argv[1] == 'clean':
            cls.argv['action'] = 'clean'

    def get(cls, name):
        return cls.argv[name]


################################################################################
# Main
#

ToolManual = """\
Usage:
  vimit build dir1 [dir2 dir3 ...]
  vimit install dir
  vimit clean
"""

if __name__ == "__main__":
    parser = ArgumentParser(sys.argv)

    if parser.get('action') == '':
        print parser.message
        sys.exit()

    if parser.get('action') == 'build':
        builder = Builder(parser.get('project-entries'))
        size = builder.build()
        print "\nProject (total: {}) is built completely.".format(hSize(size))
        print "You can vim it now.\n"
    elif parser.get('action') == 'install':
        installer = Installer(parser.get('install-folder'))
        isOk = installer.install()

        if isOk:
            print "Install successfully.\n"
        else:
            print "Abort.\n"
    elif parser.get('action') == 'clean':
        size = Builder.clean()
        print "\nDone. Free {}.\n".format(hSize(size))
